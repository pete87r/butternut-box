## Calendar Task

### Solution

Components has been divided into 3 groups:

**Base**
- Backdrop
- Button
- Typography

**Calendar**
- DatePicker
- CalendarModal

**Delivery**
- DeliveryButton
- DeliveryCard

Delivery group returns initial state - card with title and button to open calendar modal.

Calendar group returns calendal modal which appears after click on button in delivery card.

Components communication base on passing props, I decided to not use any global state management method (redux, context ect.) because components are close each other.

DatePicker is organized by CSS grid, selected date is stored in parent's component state (CalendarModal).

Application is deployed [here](https://butternut-box.web.app/).

Storybooks are available [here](https://butternut-box-storybooks.web.app),
