import '../src/index.sass'

export const decorators = [
  Story => {
    return (     
      <Story />
    )
  }
]

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: { expanded: true },
}
