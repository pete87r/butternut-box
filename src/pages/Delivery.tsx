import { FC, useState } from 'react';
import DeliveryCard from '../components/delivery/DeliveryCard';
import { Backdrop } from '../components/base';
import CalendarModal from '../components/calendar/CalendarModal';
import { useMediaQuery } from 'react-responsive';
import './Delivery.sass';

const Delivery: FC = () => {
  const currentDate = new Date();
  const isMobileView = useMediaQuery({ query: '(max-width: 550px)' });

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [isClosing, setIsClosing] = useState<boolean>(false);
  const [selectedDay, setSelectedDay] = useState<number>(currentDate.getDate());

  const handleChangeDate = (day: number) => {
    setSelectedDay(day);
    handleCloseModal();
  };

  const handleCloseModal = () => {
    setTimeout(() => {
      setOpenModal(false);
      setIsClosing(false);
    }, 1000);
    setIsClosing(true);
  };

  return (
    <div className="delivery-root">
      <DeliveryCard
        onClick={() => setOpenModal(true)}
        selectedDay={selectedDay}
      />
      {openModal && (
        <>
          {!isMobileView && (
            <Backdrop onClick={handleCloseModal} isClosing={isClosing} />
          )}
          <CalendarModal
            onChange={handleChangeDate}
            onCancel={handleCloseModal}
            isClosing={isClosing}
            initialDay={selectedDay}
          />
        </>
      )}
    </div>
  );
};

export default Delivery;
