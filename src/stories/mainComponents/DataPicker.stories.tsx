import { Story, Meta } from '@storybook/react';
import DatePicker, {
  DatePickerProps,
} from '../../components/calendar/DatePicker';
import { getCurrentDay } from '../../helpers/dateHelper';

export default {
  title: 'Components/Main/DatePicker',
  component: DatePicker,
  argTypes: {
    onDayChange: { action: 'clicked' },
  },
} as Meta;

const Template: Story<DatePickerProps> = (args) => <DatePicker {...args} />;

export const Default = Template.bind({});
Default.args = {
  selectedDay: getCurrentDay(),
};
