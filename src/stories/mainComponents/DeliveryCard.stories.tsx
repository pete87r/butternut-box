import { Story, Meta } from '@storybook/react';
import DeliveryCard, {
  DeliveryCardProps,
} from '../../components/delivery/DeliveryCard';
import { getCurrentDay } from '../../helpers/dateHelper';

export default {
  title: 'Components/Main/DeliveryCard',
  component: DeliveryCard,
  argTypes: {
    onClick: { action: 'clicked' },
  },
} as Meta;

const Template: Story<DeliveryCardProps> = (args) => <DeliveryCard {...args} />;

export const Default = Template.bind({});
Default.args = {
  selectedDay: getCurrentDay(),
};
