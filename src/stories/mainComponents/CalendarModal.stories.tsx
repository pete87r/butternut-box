import { Story, Meta } from '@storybook/react';
import CalendarModal, {
  CalendarProps,
} from '../../components/calendar/CalendarModal';
import { getCurrentDay } from '../../helpers/dateHelper';

export default {
  title: 'Components/Main/CalendarModal',
  component: CalendarModal,
  argTypes: {
    onChange: { action: 'clicked' },
    onCancel: { action: 'clicked' },
  },
} as Meta;

const Template: Story<CalendarProps> = (args) => <CalendarModal {...args} />;

export const Default = Template.bind({});
Default.args = {
  isClosing: false,
  initialDay: getCurrentDay(),
};
