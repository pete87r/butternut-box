import { Story, Meta } from '@storybook/react';
import DeliveryButton, {
  DeliveryButtonProps,
} from '../../components/delivery/DeliveryButton';
import { getCurrentDay } from '../../helpers/dateHelper';

export default {
  title: 'Components/Main/DeliveryButton',
  component: DeliveryButton,
  argTypes: {
    onClick: { action: 'clicked' },
  },
} as Meta;

const Template: Story<DeliveryButtonProps> = (args) => (
  <DeliveryButton {...args} />
);

export const Default = Template.bind({});
Default.args = {
  selectedDay: getCurrentDay(),
};
