import { Story, Meta } from '@storybook/react';
import Button, { ButtonProps } from '../../components/base/Button';

export default {
  title: 'Components/Base/Button',
  component: Button,
  argTypes: {
    onClick: { action: 'clicked' },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Default = Template.bind({});
Default.args = {
  label: 'Click Me!',
  type: 'box',
};
