import { Story, Meta } from '@storybook/react';

import Backdrop, { BackdropProps } from '../../components/base/Backdrop';

export default {
  title: 'Components/Base/Backdrop',
  component: Backdrop,
} as Meta;

const Template: Story<BackdropProps> = (args) => (
  <Backdrop {...args}>Some dummy text...</Backdrop>
);

export const Default = Template.bind({});
Default.args = {};
