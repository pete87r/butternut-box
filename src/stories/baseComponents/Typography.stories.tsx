import { Story, Meta } from '@storybook/react';

import Typography, { TypographyProps } from '../../components/base/Typography';

export default {
  title: 'Components/Base/Typography',
  component: Typography,
} as Meta;

const Template: Story<TypographyProps> = (args) => (
  <Typography {...args}>Welcome to the Storybook</Typography>
);

export const Default = Template.bind({});
Default.args = {
  variant: 'title',
  color: 'blue',
};
