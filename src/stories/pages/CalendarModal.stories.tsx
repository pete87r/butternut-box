import { Story, Meta } from '@storybook/react';
import Delivery from '../../pages/Delivery';

export default {
  title: 'Pages/Delivery',
  component: Delivery,
} as Meta;

const Template: Story<never> = (args) => <Delivery {...args} />;

export const Default = Template.bind({});
