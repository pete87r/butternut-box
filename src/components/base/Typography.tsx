import { FC } from 'react';
import './Typography.sass';

export interface TypographyProps {
  color?: 'red' | 'blue' | 'white' | 'gray';
  variant: 'title' | 'body1' | 'body2' | 'body3';
  extraStyle?: string;
}

const Typography: FC<TypographyProps> = ({
  children,
  variant,
  color = 'blue',
  extraStyle,
}) => {
  return (
    <p
      className={`'text' ${variant} ${color} ${extraStyle}`}
      data-testid={`typography-${variant}-${color}`}
    >
      {children}
    </p>
  );
};

export default Typography;
