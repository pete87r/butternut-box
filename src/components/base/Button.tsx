import { FC, MouseEventHandler } from 'react';
import './Button.sass';

export interface ButtonProps {
  label: string | number;
  onClick: MouseEventHandler<HTMLButtonElement>;
  type: 'box' | 'text';
}

const Button: FC<ButtonProps> = ({ label, onClick, type = 'box' }) => {
  return (
    <button
      data-testid={`button-${type}`}
      type="button"
      onClick={onClick}
      className={`button-${type}`}
    >
      {label}
    </button>
  );
};

export default Button;
