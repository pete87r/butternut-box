import { FC, MouseEventHandler } from 'react';
import './Backdrop.sass';

export interface BackdropProps {
  onClick: MouseEventHandler<HTMLDivElement>;
  isClosing: boolean;
}

const Backdrop: FC<BackdropProps> = ({ onClick, isClosing }) => {
  return (
    <div
      data-testid="backdrop"
      className={`backdrop ${isClosing && 'close-backdrop-animation'}`}
      onClick={onClick}
    />
  );
};

export default Backdrop;
