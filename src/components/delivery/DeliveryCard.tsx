import { FC, MouseEventHandler } from 'react';
import './DeliveryCard.sass';
import { Typography } from '../base';
import DeliveryButton from './DeliveryButton';

export interface DeliveryCardProps {
  selectedDay: number;
  onClick: MouseEventHandler<HTMLButtonElement>;
}

const DeliveryCard: FC<DeliveryCardProps> = ({ selectedDay, onClick }) => {
  return (
    <div className="delivery-card" data-testid="delivery-card">
      <div className="delivery-card-main-text-container">
        <Typography variant="title" extraStyle="delivery-card-title">
          Choose your delivery day
        </Typography>
        <div className="delivery-card-text-container">
          <Typography variant="body2">Delivery is always free</Typography>
        </div>
      </div>
      <DeliveryButton onClick={onClick} selectedDay={selectedDay} />
    </div>
  );
};

export default DeliveryCard;
