import { FC, MouseEventHandler } from 'react';
import { Typography } from '../base';
import VanIcon from '../../icons/van.svg';
import CalendarIcon from '../../icons/calendar.svg';
import {
  getWorkdayMonthDay,
  isSelectedDateEarliest,
} from '../../helpers/dateHelper';
import './DeliveryButton.sass';

export interface DeliveryButtonProps {
  selectedDay: number;
  onClick: MouseEventHandler<HTMLButtonElement>;
}

const DeliveryButton: FC<DeliveryButtonProps> = ({ selectedDay, onClick }) => {
  const isEarliestDelivery = isSelectedDateEarliest(selectedDay);

  return (
    <button
      className="delivery-button"
      onClick={onClick}
      data-testid="delivery-button"
    >
      <div className="delivery-button-background" />
      <div className="delivery-button-body">
        <div className="delivery-button-delivery-container">
          <Typography
            variant={isEarliestDelivery ? 'body2' : 'body1'}
            extraStyle="delivery-button-date-text"
          >
            {getWorkdayMonthDay(selectedDay)}
          </Typography>
          {isEarliestDelivery && (
            <div className="delivery-button-van-container">
              <img src={VanIcon} alt="Van" width="22" data-testid="van-icon" />
              <Typography variant="body3" extraStyle="delivery-button-van-text">
                Earliest Delivery
              </Typography>
            </div>
          )}
        </div>
        <div className="delivery-button-calendar-container">
          <img
            src={CalendarIcon}
            alt="Calendar"
            width="26"
            data-testid="calendar-icon"
          />
          <Typography variant="body2" extraStyle="delivery-button-day-text">
            {selectedDay}
          </Typography>
          <Typography variant="body2">Change</Typography>
        </div>
      </div>
    </button>
  );
};

export default DeliveryButton;
