import { FC, useCallback } from 'react';
import './DatePicker.sass';
import { Typography } from '../base';
import WhiteVanIcon from '../../icons/whiteVan.svg';
import {
  getMonthLength,
  getCurrentDay,
  getFirstDayOfMonth,
} from '../../helpers/dateHelper';

export interface DatePickerProps {
  selectedDay: number;
  onDayChange: Function;
}

export const weekDays = ['M', 'T', 'W', 'T', 'F', 'S', 'S'];

const DatePicker: FC<DatePickerProps> = ({ selectedDay, onDayChange }) => {
  const getSelectedCell = useCallback((day: number) => {
    return (
      <div
        className="date-picker-inner-cell-selected"
        data-testid={`selected-day-${day}-cell`}
      >
        <Typography
          variant="body2"
          color="white"
          extraStyle="selected-cell-text"
        >
          {day}
        </Typography>
        <img
          data-testid={`white-van-icon-${day}`}
          src={WhiteVanIcon}
          alt="Van"
          width="26"
          className="selected-cell-icon"
        />
      </div>
    );
  }, []);

  const getEnabledCells = useCallback(
    (selected: boolean, day: number) => {
      return (
        <div
          data-testid={`enabled-cell`}
          className={`date-picker-cell
                  ${
                    selected
                      ? 'date-picker-cell-selected'
                      : 'date-picker-body-cell'
                  }`}
          onClick={() => onDayChange(day)}
        >
          {selected ? (
            getSelectedCell(day)
          ) : (
            <Typography variant="body2" color={selected ? 'white' : 'red'}>
              {day}
            </Typography>
          )}
        </div>
      );
    },
    [onDayChange, getSelectedCell]
  );

  const getDisabledCells = (day: number) => {
    return (
      <div
        className={`date-picker-cell date-picker-body-cell-disabled`}
        data-testid={`disabled-cell`}
      >
        <Typography variant="body2" color="gray">
          {day}
        </Typography>
      </div>
    );
  };

  return (
    <div className="date-picker-container" data-testid={`date-picker`}>
      {weekDays.map((day, index) => (
        <div
          className={`date-picker-cell date-picker-header-cell`}
          key={`${day}-${index}`}
        >
          <Typography variant="body2">{day}</Typography>
        </div>
      ))}
      {[...Array(getMonthLength() + getFirstDayOfMonth())].map(
        (cell, index) => {
          const day = index - getFirstDayOfMonth() + 1;
          const selected = selectedDay === day;
          return (
            <div key={day}>
              {index >= getFirstDayOfMonth() &&
                (index >= getCurrentDay()
                  ? getEnabledCells(selected, day)
                  : getDisabledCells(day))}
            </div>
          );
        }
      )}
    </div>
  );
};

export default DatePicker;
