import { FC, MouseEventHandler, useState } from 'react';
import './CalendarModal.sass';
import { Button, Typography } from '../base';
import DatePicker from './DatePicker';
import { getMonthYear } from '../../helpers/dateHelper';

export interface CalendarProps {
  isClosing: boolean;
  onChange: Function;
  onCancel: MouseEventHandler<HTMLButtonElement>;
  initialDay: number;
}

const Calendar: FC<CalendarProps> = ({
  isClosing,
  onChange,
  onCancel,
  initialDay,
}) => {
  const [selectedDay, setSelectedDay] = useState<number>(initialDay);

  return (
    <div
      data-testid="calendar-modal"
      className={`calendar-modal ${
        isClosing && 'close-calendar-modal-animation'
      }`}
    >
      <Typography variant="title">{getMonthYear()}</Typography>
      <DatePicker
        onDayChange={(day: number) => setSelectedDay(day)}
        selectedDay={selectedDay}
      />
      <div className="calendar-modal-buttons-container">
        <Button label="cancel, don't change" type="text" onClick={onCancel} />
        <Button
          label="change date"
          type="box"
          onClick={() => onChange(selectedDay)}
        />
      </div>
    </div>
  );
};

export default Calendar;
