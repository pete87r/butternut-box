import { CalendarModal } from '../../components/calendar';
import { fireEvent, render } from '@testing-library/react';
import { getMonthYear } from '../../helpers/dateHelper';

describe('Component:Calendar:CalendarModal', () => {
  test('should render calendar modal and handle on change click', () => {
    const cancel = jest.fn();
    const change = jest.fn();

    const { getByText, getByTestId } = render(
      <CalendarModal
        isClosing={false}
        onChange={change}
        onCancel={cancel}
        initialDay={15}
      />
    );
    getByTestId('calendar-modal');
    getByTestId('date-picker');
    getByTestId('white-van-icon-15');
    getByTestId('button-box');
    getByTestId('button-text');
    getByText(getMonthYear());
    getByText("cancel, don't change");
    getByText('change date');
    fireEvent.click(getByTestId('button-box'));
    expect(change).toHaveBeenCalledTimes(1);
    expect(cancel).toHaveBeenCalledTimes(0);
  });

  test('should render calendar modal and handle on cancel click', () => {
    const cancel = jest.fn();
    const change = jest.fn();

    const { getByTestId } = render(
      <CalendarModal
        isClosing={false}
        onChange={change}
        onCancel={cancel}
        initialDay={15}
      />
    );
    getByTestId('calendar-modal');
    fireEvent.click(getByTestId('button-text'));
    expect(change).toHaveBeenCalledTimes(0);
    expect(cancel).toHaveBeenCalledTimes(1);
  });
});
