import DatePicker, { weekDays } from '../../components/calendar/DatePicker';
import { fireEvent, render } from '@testing-library/react';

describe('Component:Calendar:DatePicker', () => {
  jest.useFakeTimers('modern').setSystemTime(new Date('2021-06-01'));

  test('should render date picker and react on date changing', async () => {
    const mockFn = jest.fn();

    const {
      getByText,
      getByTestId,
      getAllByText,
      getAllByTestId,
      queryByTestId,
    } = render(<DatePicker onDayChange={mockFn} selectedDay={1} />);
    getByTestId('date-picker');
    getByTestId('white-van-icon-1');
    getByTestId('selected-day-1-cell');
    weekDays.map((weekday) => getAllByText(weekday));
    expect(queryByTestId('disabled-cell')).toBeFalsy();
    expect(getAllByTestId('enabled-cell')).toHaveLength(30);
    fireEvent.click(getByText('15'));
    expect(mockFn).toHaveBeenCalledTimes(1);
  });
});
