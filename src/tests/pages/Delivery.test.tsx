import Delivery from '../../pages/Delivery';
import { fireEvent, render, waitFor } from '@testing-library/react';

describe('Page:Delivery', () => {
  jest.useFakeTimers('modern').setSystemTime(new Date('2021-06-01'));

  test('should render delivery page and open and close calendar modal on click', async () => {
    const { getByTestId, queryByTestId, getByText } = render(<Delivery />);
    getByTestId('delivery-card');
    expect(queryByTestId('calendar-modal')).toBeFalsy();
    getByText('Tue June 01');
    getByText('1');
    fireEvent.click(getByTestId('delivery-button'));
    fireEvent.click(getByText('23'));
    getByTestId('calendar-modal');
    fireEvent.click(getByTestId('button-box'));
    getByText('Wed June 23');
    getByText('15');
  });
});
