import { Backdrop } from '../../components/base';
import { fireEvent, render } from '@testing-library/react';

describe('Component:Base:Backdrop', () => {
  test('should render backdrop', () => {
    const mockFn = jest.fn();

    const { getByTestId } = render(
      <Backdrop onClick={mockFn} isClosing={false} />
    );
    getByTestId('backdrop');
    fireEvent.click(getByTestId('backdrop'));
    expect(mockFn).toHaveBeenCalledTimes(1);
  });
});
