import { Typography } from '../../components/base';
import { render } from '@testing-library/react';

describe('Component:Base:Button', () => {
  test('should render title typography with default color', () => {
    const { getByText, getByTestId } = render(
      <Typography variant="title">Test</Typography>
    );
    getByTestId('typography-title-blue');
    getByText('Test');
  });

  test('should render body1 typography with red color', () => {
    const { getByText, getByTestId } = render(
      <Typography variant="body1" color="red">
        Test
      </Typography>
    );
    getByTestId('typography-body1-red');
    getByText('Test');
  });
});
