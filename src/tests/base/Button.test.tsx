import { Button } from '../../components/base';
import { fireEvent, render } from '@testing-library/react';

describe('Component:Base:Button', () => {
  test('should render box button and handle onClick', () => {
    const mockFn = jest.fn();

    const { getByText, getByTestId } = render(
      <Button onClick={mockFn} label="Test" type="box" />
    );
    getByTestId('button-box');
    getByText('Test');
    fireEvent.click(getByTestId('button-box'));
    expect(mockFn).toHaveBeenCalledTimes(1);
  });

  test('should render text button and handle onClick', () => {
    const mockFn = jest.fn();

    const { getByText, getByTestId } = render(
      <Button onClick={mockFn} label="Test" type="text" />
    );
    getByTestId('button-text');
    getByText('Test');
    fireEvent.click(getByTestId('button-text'));
    expect(mockFn).toHaveBeenCalledTimes(1);
  });
});
