import { DeliveryCard } from '../../components/delivery';
import { fireEvent, render } from '@testing-library/react';

describe('Component:Delivery:DeliveryCard', () => {
  test('should render delivery card', () => {
    const mockFn = jest.fn();

    const { getByTestId, getByText } = render(
      <DeliveryCard onClick={mockFn} selectedDay={15} />
    );
    getByTestId('delivery-card');
    getByTestId('delivery-button');
    getByText('Choose your delivery day');
    getByText('Delivery is always free');
    getByText('15');
    fireEvent.click(getByTestId('delivery-button'));
    expect(mockFn).toHaveBeenCalledTimes(1);
  });
});
