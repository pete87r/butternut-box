import { DeliveryButton } from '../../components/delivery';
import { fireEvent, render } from '@testing-library/react';
import { getWorkdayMonthDay } from '../../helpers/dateHelper';

describe('Component:Delivery:DeliveryButton', () => {
  jest.useFakeTimers('modern').setSystemTime(new Date('2021-06-01'));

  test('should render delivery button with earliest delivery option', () => {
    const mockFn = jest.fn();

    const { getByTestId, getByText } = render(
      <DeliveryButton onClick={mockFn} selectedDay={1} />
    );
    getByTestId('delivery-button');
    getByTestId('van-icon');
    getByTestId('calendar-icon');
    getByText('Earliest Delivery');
    getByText(1);
    getByText(getWorkdayMonthDay(1));
    getByText('Change');
    fireEvent.click(getByTestId('delivery-button'));
    expect(mockFn).toHaveBeenCalledTimes(1);
  });

  test('should render delivery button without earliest delivery option', () => {
    const mockFn = jest.fn();

    const { getByTestId, getByText, queryByText } = render(
      <DeliveryButton onClick={mockFn} selectedDay={2} />
    );
    getByTestId('delivery-button');
    expect(queryByText('van-icon')).toBeFalsy();
    expect(queryByText('Earliest Delivery')).toBeFalsy();
    getByTestId('calendar-icon');
    getByText(2);
    getByText(getWorkdayMonthDay(2));
    getByText('Change');
    fireEvent.click(getByTestId('delivery-button'));
    expect(mockFn).toHaveBeenCalledTimes(1);
  });
});
