const date = new Date();

export const getWorkdayMonthDay = (selectedDay?: number) => {
  selectedDay && date.setDate(selectedDay);

  let day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);
  let weekday = new Intl.DateTimeFormat('en', { weekday: 'short' }).format(
    date
  );

  return `${weekday} ${getFormatMonth()} ${day}`;
};

export const getMonthYear = () => {
  return `${getFormatMonth()} ${date.getFullYear()}`;
};

export const getMonthLength = () => {
  const year = date.getFullYear();
  const month = date.getMonth();
  return new Date(year, month + 1, 0).getDate();
};

export const getCurrentDay = () => {
  return new Date().getDate();
};

export const getFirstDayOfMonth = () => {
  date.setDate(1);
  return date.getDay() - 1;
};

export const isSelectedDateEarliest = (selectedDate: number) => {
  return new Date().getDate() === selectedDate;
};

const getFormatMonth = () => {
  return new Intl.DateTimeFormat('en', { month: 'long' }).format(date);
};
